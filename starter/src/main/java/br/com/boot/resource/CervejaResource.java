package br.com.boot.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.boot.model.Cerveja;
import br.com.boot.repository.CervejaRepository;

@RestController
public class CervejaResource {

	@Autowired
	private CervejaRepository cervejaRepository;

	@RequestMapping(value = "/cervejas", method = RequestMethod.GET)
	public ResponseEntity<List<Cerveja>> listar() {
		return new ResponseEntity<List<Cerveja>>(cervejaRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/cervejas/{id}")
	public ResponseEntity<Response<Cerveja>> buscarPorid(@PathVariable Long id) {
		Optional<Cerveja> cerveja = cervejaRepository.findById(id);
		Response<Cerveja> response = new Response<Cerveja>();
		if(!cerveja.isPresent()) {
			response.getErrors().add("Cerveja não encontrada: " + id);
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(cerveja.get());
		return ResponseEntity.ok(response);
	}
}
