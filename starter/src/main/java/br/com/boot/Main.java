package br.com.boot;

import java.util.Scanner;

public class Main {

	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int index = 0;
        int countString = scanner.nextInt();
        
        while(scanner.hasNext() && index < countString) {
            String s = scanner.next();
            String even = new String("");
            String odd = new String("");
            for(int i = 0; i < s.length(); i++){
                if(i % 2 == 0){
                    even = "" + even + s.charAt(i);
                }else{
                    odd = "" + odd + s.charAt(i);
                }
            }
            
            System.out.println("" + even + " " + odd);
            
            index++;
        }
        scanner.close();
	}

}
